package ru.tsc.tambovtsev.tm.service;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.service.IProjectService;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.model.Task;
import ru.tsc.tambovtsev.tm.repository.AbstractRepository;
import ru.tsc.tambovtsev.tm.repository.ProjectRepository;

import java.util.*;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IProjectRepository repository) {
        super(repository);
    }

    @Nullable
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        return repository.create(userId, name);
    }

    @Nullable
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        return repository.create(userId, name, description);
    }

    @Nullable
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name,
                          @Nullable final String description, @Nullable final Date dateBegin,
                          @Nullable final Date dateEnd) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        final Project project = create(userId, name, description);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        return project;
    }

    @Nullable
    @Override
    public Project updateById(@Nullable final String userId, @Nullable final String id,
                              @Nullable final String name, @Nullable final String description) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        final Project project = findById(userId, id);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return project;
    }

    @Nullable
    @Override
    public Project changeProjectStatusById(@Nullable final String userId, @Nullable final String id,
                                           @Nullable final Status status) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        final Project project = findById(userId, id);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        project.setUserId(userId);
        return project;
    }

}
