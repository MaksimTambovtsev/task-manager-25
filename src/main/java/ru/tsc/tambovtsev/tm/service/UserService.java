package ru.tsc.tambovtsev.tm.service;

import com.sun.org.apache.bcel.internal.generic.ARETURN;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.repository.IUserRepository;
import ru.tsc.tambovtsev.tm.api.service.IPropertyService;
import ru.tsc.tambovtsev.tm.api.service.IUserService;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.tambovtsev.tm.model.Task;
import ru.tsc.tambovtsev.tm.model.User;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.util.HashUtil;

import javax.jws.soap.SOAPBinding;
import java.util.List;
import java.util.Optional;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    @Nullable
    private final IProjectRepository projectRepository;

    @Nullable
    private final ITaskRepository taskRepository;

    public UserService(
            @Nullable final IPropertyService propertyService,
            @Nullable final IUserRepository userRepository,
            @Nullable final ITaskRepository taskRepository,
            @Nullable final IProjectRepository projectRepository
    ) {
        super(userRepository);
        this.propertyService = propertyService;
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        return repository.findByLogin(login);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        return repository.findByEmail(email);
    }

    @Nullable
    @Override
    public User remove(@Nullable final User model) throws AbstractException {
        if (model == null) return null;
        final User user = super.remove(model);
        if (user == null) return null;
        @Nullable final String userId = user.getId();
        taskRepository.clear(userId);
        projectRepository.clear(userId);
        return user;
    }

    @Nullable
    @Override
    public User removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        return repository.removeByLogin(login);
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Nullable
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        if (isLoginExists(login)) throw new LoginExistsException();
        @Nullable final User user = new User();
        user.setRole(Role.USUAL);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return repository.add(user);
    }

    @Nullable
    @Override
    public User create(@Nullable final String login, @Nullable final String password,
                       @Nullable final String email) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        if (isEmailExists(email)) throw new EmailExistsException();
        @Nullable final User user = create(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return repository.add(user);
    }

    @Nullable
    @Override
    public User create(final String login, final String password, final Role role) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        Optional.ofNullable(role).orElseThrow(RoleEmptyException::new);
        @Nullable final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return repository.add(user);
    }

    @Nullable
    @Override
    public User setPassword(@Nullable final String userId, @Nullable final String password) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        final User user = findById(userId);
        if (user == null) return null;
        final String hash = HashUtil.salt(propertyService, password);
        user.setPasswordHash(hash);
        return user;
    }

    @Nullable
    @Override
    public User updateUser(@Nullable final String userId, @Nullable final String firstName,
                           @Nullable final String lastName, @Nullable final String middleName) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        @Nullable final User user = findById(userId);
        Optional.ofNullable(firstName).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(lastName).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(middleName).orElseThrow(NameEmptyException::new);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return null;
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @Nullable final User user = findByLogin(login);
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @Nullable final User user = findByLogin(login);
        user.setLocked(false);
    }

}
